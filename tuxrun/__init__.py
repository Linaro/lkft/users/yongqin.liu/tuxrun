# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2021-present Linaro Limited
#
# SPDX-License-Identifier: MIT

"""
Command line tool for testing Linux under QEMU
"""
__version__ = "0.30.0"
